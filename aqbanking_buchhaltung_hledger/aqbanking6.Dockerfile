# First stage, build buchhaltung
# stack-build:lts-9.9 didn't work
FROM fpco/stack-build:lts-11 as stackbuild

RUN git clone https://github.com/johannesgerer/buchhaltung.git /usr/src/buchhaltung
WORKDIR /usr/src/buchhaltung
RUN  stack build --system-ghc

# Second stage

FROM debian:stretch
MAINTAINER varac@varac.net
ENV DEBIAN_FRONTEND noninteractive

COPY --from=stackbuild /usr/src/buchhaltung/.stack-work/install/x86_64-linux/lts-12.11/8.4.3/bin/buchhaltung /usr/bin/

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    git \
    locales \
    ca-certificates \
    curl \
    # Generic build deps
    autoconf \
    automake \
    build-essential \
    libtool \
    # Build deps for gwenhyfar
    libgcrypt20-dev \
    libgnutls28-dev \
    # Build deps for ?
    libxmlsec1-gnutls \
    libxmlsec1-dev \
    # Run deps for buchhaltung
    dbacl && \
    apt-get clean && rm -rf /var/lib/apt/lists/


RUN git clone https://github.com/aqbanking/aqbanking.git /usr/local/src/aqbanking && \
    git clone https://github.com/aqbanking/gwenhywfar.git /usr/local/src/gwenhywfar

WORKDIR /usr/local/src/gwenhywfar
# Checkout latest tag
RUN  git checkout "$(git describe --tags --abbrev=0)" && \
  # WORKAROUND FOR AQBANKING „THE TLS CONNECTION WAS NON-PROPERLY TERMINATED“
  # https://blog.mcbachmann.de/workaround/workaround-for-aqbanking-the-tls-connection-was-non-properly-terminated
  sed -e 's/rv==GNUTLS_E_PREMATURE_TERMINATION/rv!=GNUTLS_E_PREMATURE_TERMINATION/' -i src/sio/syncio_tls.c && \
  make -f Makefile.cvs && \
  ./configure --with-guis=none && \
  make && \
  make install

WORKDIR /usr/local/src/aqbanking
# Checkout latest tag
RUN git checkout "$(git describe --tags --abbrev=0)" && \
  export ACLOCAL_FLAGS='-I /usr/share/aclocal' && \
  make -f Makefile.cvs && \
  ./configure --enable-cli --with-backends=aqhbci --with-bankinfos=de && \
  make && \
  make install

RUN apt-get purge -y autoconf automake build-essential libtool libgcrypt20-dev libgnutls28-dev libxmlsec1-gnutls libxmlsec1-dev

RUN groupadd --gid 1000 varac && useradd --create-home --gid varac --uid 1000 varac
USER varac:varac
WORKDIR /home/varac
