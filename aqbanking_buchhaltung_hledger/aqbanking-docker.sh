#!/usr/bin/env sh

set -eu

docker run \
       --rm \
       --name="aqbanking_$(date +%s)" \
       --hostname aqbanking_container \
       --interactive \
       --tty \
       --volume "$HOME/.aqbanking":/home/varac/.aqbanking \
       --volume "$HOME/.buchhaltung":/home/varac/.buchhaltung \
       --volume "$HOME/hledger":/home/varac/hledger \
       varac/aqbanking:5 bash

#podman run \
#       --userns=keep-id \
#       --rm \
#       --name="aqbanking_$(date +%s)" \
#       --hostname aqbanking_container \
#       --interactive \
#       --tty \
#       --volume "$HOME/.aqbanking":/home/varac/.aqbanking \
#       --volume "$HOME/.buchhaltung":/home/varac/.buchhaltung \
#       --volume "$HOME/hledger":/home/varac/hledger \
#       varac/aqbanking:5 bash
